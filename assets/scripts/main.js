// @codekit-prepend "_plugins.js";

$(document).ready(function() {

	// Magnific Popup
	$("ul.js-picturesGrid").magnificPopup({
		delegate: 'a',
		type: 'image',
		mainClass: 'mfp-fade',
		gallery: {
		  enabled: true
		}

	});

	// Animating in page scroll
	$('a.js-navLink').on('click', function(event) {
		event.preventDefault();

		$('html, body').animate({
			scrollTop: $($(this).attr('href')).offset().top
		}, 400);
		$('#siteNav').removeClass('is-open');
	});

	// Mobile nav
	$('#menuOpenIcon').on('click', function() {
		$('#siteNav').addClass('is-open');
	});

	$('#menuCloseIcon').on('click', function() {
		$('#siteNav').removeClass('is-open');
	});

});

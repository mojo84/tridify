<!DOCTYPE html>
<html lang="en">

<head>

    <script>
        WebFontConfig = {
        google: {
          families: ['Roboto:300,300italic,400,400italic,500,500italic,700,700italic']
        }
      };

       (function(d) {
          var wf = d.createElement('script'), s = d.scripts[0];
          wf.src = 'assets/scripts/min/webfontloader-min.js';
          s.parentNode.insertBefore(wf, s);
       })(document);
    </script>


    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Always force latest IE rendering engine (even in intranet) -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Tridify</title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <script>
      // Picture element HTML5 shiv
      document.createElement( "picture" );
    </script>
    <script src="assets/scripts/min/picturefill-min.js" async></script>


    <!-- CSS files -->
    <link rel="stylesheet" href="assets/styles/css/main.css">

</head>


<body>

  <header class="c-page-header">
    <div class="o-wrapper c-page-header__inner">
      <a class="c-site-logo" href="/">
         <h1>
           <img src="assets/images/logo@2x.png" width="172" height="74" alt="">
         </h1>
      </a>
      <nav class="c-navbar">
        <button id="menuOpenIcon" type="button" class="c-menu-icon"><i class="icon-menu"></i></button>

        <ul id="siteNav" class="c-site-nav o-list-inline">
          <li class="c-site-nav__item c-site-nav__item--close">
            <button id="menuCloseIcon" type="button" class="c-menu-icon"><i class="icon-cross"></i></button>
          </li>

          <li class="c-site-nav__item o-list-inline__item">
            <a class="c-site-nav__link" href="/">Home</a>
          </li>

          <li class="c-site-nav__item o-list-inline__item">
            <a class="c-site-nav__link js-navLink" href="#section1">Vision</a>
          </li>

          <li class="c-site-nav__item o-list-inline__item">
            <a class="c-site-nav__link js-navLink" href="#section2">Service</a>
          </li>

          <li class="c-site-nav__item o-list-inline__item">
            <a class="c-site-nav__link js-navLink" href="#section3">Product</a>
          </li>

          <li class="c-site-nav__item o-list-inline__item">
            <a class="c-site-nav__link js-navLink" href="#section4">About us</a>
          </li>

	        <!-- <li class="c-site-nav__item o-list-inline__item">
            <a class="c-site-nav__link js-navLink" href="#section5">Video</a>
          </li> -->

          <li class="c-site-nav__item o-list-inline__item">
            <a class="c-site-nav__link js-navLink" href="#contact">Contact</a>
          </li>
        </ul>
        <!-- /.c-site-nav -->
      </nav>
      <!-- /.c-navbar -->
    </div>
    <!-- /.o-wrapper c-page-header__inner -->
  </header>
  <!-- /.c-page-header -->


	<main role="main">

		<div class="c-section-wrapper">

					<div class="c-hero">

            <div class="c-hero__img c-hero__img--overlay">
        	    <picture>
    			      <source srcset="assets/images/hero2@1280x590.jpg" media="(min-width: 1180px)">
    			      <source srcset="assets/images/hero2@1180x440.jpg" media="(min-width: 740px)">
    			      <img srcset="assets/images/hero2@740x240.jpg" alt="">
    			    </picture>
        	  </div>
						<!-- /.c-hero__img -->

						<div class="c-hero__body">

							<div class="o-wrapper">
								<div class="c-hero__text editable">
									<p>If buildings are for people</p>
									<p>Shouldn’t everyone have a view?</p>
								</div>
							</div>
							<!-- /.o-wrapper -->

						</div>
						<!-- /.c-hero__body-->

					</div>
					<!-- /.c-hero -->

				</div>
				<!-- /.c-section-wrapper -->

		<div id="section1" class="c-section-wrapper">

							<div class="c-section u-padding-bottom-none">
								<div class="o-wrapper">

									<div class="c-section__header">
										<h2 class="c-section__heading editable-text">Experience your vision of the future</h2>
										<div class="c-section__copy editable">
    									<p>Aligning the latest BIM, virtual and augmented reality technologies with your key data, Tridify works closely with you to bring your designs to life so you can meet the many challenges and grasp the opportunities every project brings.</p>
    									<p>To create better, more sustainable places to live and work, you need to be sure your vision will deliver in the real world. That your designs will truly match your finished project, on time, on spec and on budget.</p>
    				          <p>Live your vision…with Tridify</p>
    								</div>
										<!-- /.c-section__copy -->
									</div>
									<!-- /.c-section__header -->

								</div>
								<!-- /.o-wrapper -->
							</div>
							<!-- /.c-section -->

						</div>
						<!-- /.c-section-wrapper -->

    <div class="c-section-wrapper">

							<div class="c-section c-section--dark-bg u-padding-top-none u-padding-bottom-none">

								<div class="c-banner">

									<div class="c-banner__img">
										<img src="assets/images/banner-img6.jpg" alt="">
									</div>
									<!-- /.c-banner__img -->

								</div>
								<!-- /.c-banner -->

							</div>
							<!-- /.c-section -->

						</div>
						<!-- /.c-section-wrapper -->

		<div id="section2" class="c-section-wrapper">

									<div class="c-section c-section--dark-bg">
										<div class="o-wrapper">

											<div class="c-section__header">
												<h2 class="c-section__heading editable-text">Accessible across the value chain</h2>
												<div class="c-section__copy editable">
													<p>Disruptive virtual reality technology that changes working practices.</p>
													<p>A cloud-based solution that uses BIM to provide interactive visualisations and immersive experiences to aid design and build project processes from concept to delivery and beyond.</p>
													<p>Accessible to all, anywhere, any time and on any mobile device.</p>
												</div>
												<!-- /.c-section__copy -->
											</div>
											<!-- /.c-section__header -->

											<ul class="c-features-list o-layout">
												<li class="c-features-list__item o-layout__item u-1/1 u-1/2@tablet u-1/4@desktop">
													<div class="c-features-list__box">
														<div class="c-features-list__icon">
															<img src="assets/images/icon-integration.png" alt="">
														</div>

														<h3 class="c-features-list__heading editable-text">Integrated</h3>

														<div class="c-features-list__body editable">
															<p>Platform agnostic means quick implementation</p>
														</div>
													</div>
												</li>
												<!-- /.c-features-list__item -->

												<li class="c-features-list__item o-layout__item u-1/1 u-1/2@tablet u-1/4@desktop">
													<div class="c-features-list__box">
														<div class="c-features-list__icon">
															<img src="assets/images/icon-accessibility.png" alt="">
														</div>

														<h3 class="c-features-list__heading editable-text">Accessibility</h3>

														<div class="c-features-list__body editable">
															<p>SaaS, available on all devices</p>
														</div>
													</div>
												</li>
												<!-- /.c-features-list__item -->

												<li class="c-features-list__item o-layout__item u-1/1 u-1/2@tablet u-1/4@desktop">
													<div class="c-features-list__box">
														<div class="c-features-list__icon">
															<img src="assets/images/icon-collaboration.png" alt="">
														</div>

														<h3 class="c-features-list__heading editable-text">Collaboration</h3>

														<div class="c-features-list__body editable">
															<p>Better teamwork, joined-up thinking, stronger relationships</p>
														</div>
													</div>
												</li>
												<!-- /.c-features-list__item -->

												<li class="c-features-list__item o-layout__item u-1/1 u-1/2@tablet u-1/4@desktop">
													<div class="c-features-list__box">
														<div class="c-features-list__icon">
															<img src="assets/images/icon-scalability.png" alt="">
														</div>

														<h3 class="c-features-list__heading editable-text">Scalability</h3>

														<div class="c-features-list__body editable">
															<p>From design detail up to a city</p>
														</div>
													</div>
												</li>
												<!-- /.c-features-list__item -->
											</ul>
											<!-- /.c-features-list -->

										</div>
										<!-- /.o-wrapper -->
									</div>
									<!-- /.c-section -->

								</div>
								<!-- /.c-section-wrapper -->

		<div id="section3" class="c-section-wrapper">

											<div class="c-section c-section--bg-img">
												<div class="o-wrapper">

													<div class="c-section__header c-section__header--align-left">
														<h2 class="c-section__heading c-section__heading--light-blue u-margin-bottom-none editable-text">Bring your vision to life with Tridify</h2>

														<h3 class="c-section__subheading c-section__subheading--white editable-text">Access your projects anytime, anywhere</h3>

														<div class="c-section__copy c-section__copy--white editable">
															<p class="u-margin-bottom-none">The <strong>platform agnostic</strong> nature of our technology means that it <strong>integrates seamlessly</strong> into your existing systems and devices, <strong>requiring the minimum of training</strong>, resulting in <strong>quick, effective implementation</strong>.</p>
														</div>
														<!-- /.c-section__copy -->
													</div>
													<!-- /.c-section__header -->

													<ul class="c-gallery o-layout js-picturesGrid editable">
														<li class="c-gallery__item o-layout__item u-1/1 u-1/3@mobileLandscape repeatable">
															<a class="c-gallery__thumb" href="assets/images/product-screen4.jpg">
																<img src="assets/images/product-screen-thumb4.jpg" alt="">
															</a>
														</li>
														<!-- /.c-gallery__item -->

														<li class="c-gallery__item o-layout__item u-1/1 u-1/3@mobileLandscape repeatable">
															<a class="c-gallery__thumb" href="assets/images/product-screen1.jpg">
																<img src="assets/images/product-screen-thumb1.jpg" alt="">
															</a>
														</li>
														<!-- /.c-gallery__item -->

														<li class="c-gallery__item o-layout__item u-1/1 u-1/3@mobileLandscape repeatable">
															<a class="c-gallery__thumb" href="assets/images/product-screen3.jpg">
																<img src="assets/images/product-screen-thumb3.jpg" alt="">
															</a>
														</li>
														<!-- /.c-gallery__item -->
													</ul>
													<!-- /.c-gallery -->

												</div>
												<!-- /.o-wrapper -->
											</div>
											<!-- /.c-section -->

										</div>
										<!-- /.c-section-wrapper -->

		<div id="section4" class="c-section-wrapper">

													<div class="c-section">
														<div class="o-wrapper">

															<div class="c-section__header">
																<h2 class="c-section__heading editable-text">About us</h2>
															</div>
															<!-- /.c-section__header -->

															<ul class="c-features-list o-layout">
																<li class="c-features-list__item o-layout__item u-1/1 u-1/2@tablet">
																	<div class="c-features-list__box">
																		<h3 class="c-features-list__heading editable-text">Commitment</h3>

																		<div class="c-features-list__body editable">
																			<p>We are committed to working with you to create better places for people to live and work by developing highly intuitive smart technology that drives performance, productivity and collaboration.</p>
																		</div>
																	</div>
																</li>
																<!-- /.c-features-list__item -->

																<li class="c-features-list__item o-layout__item u-1/1 u-1/2@tablet">
																	<div class="c-features-list__box">
																		<h3 class="c-features-list__heading editable-text">Integrity</h3>

																		<div class="c-features-list__body editable">
																			<p>We take great pride in our work and fully realise the major impact disruptive technology can have on your business, and the importance of making it easy to integrate, implement and use, so you get the maximum return on your investment.</p>
																		</div>
																	</div>
																</li>
																<!-- /.c-features-list__item -->

																<li class="c-features-list__item o-layout__item u-1/1 u-1/2@tablet">
																	<div class="c-features-list__box">
																		<h3 class="c-features-list__heading editable-text">Creativity</h3>

																		<div class="c-features-list__body editable">
																			<p>Our team is made up of some of the most ingenious, innovative and imaginative minds in their fields. Creativity lies at the heart of everything we do and is the foundation of our success.</p>
																		</div>
																	</div>
																</li>
																<!-- /.c-features-list__item -->

																<li class="c-features-list__item o-layout__item u-1/1 u-1/2@tablet">
																	<div class="c-features-list__box">
																		<h3 class="c-features-list__heading editable-text">Collaboration</h3>

																		<div class="c-features-list__body editable">
																			<p>Collaboration is at the core of our business and technology, so that it can bring people of different skill sets together to improve overall project performance by breaking down traditional silos.</p>
																		</div>
																	</div>
																</li>
																<!-- /.c-features-list__item -->
															</ul>
															<!-- /.c-features-list -->

														</div>
														<!-- /.o-wrapper -->
													</div>
													<!-- /.c-section -->

												</div>
												<!-- /.c-section-wrapper -->

		<div class="c-section-wrapper">

															<div class="c-section c-section--dark-bg u-padding-top-none u-padding-bottom-none">

																<div class="c-banner">

    															<div class="c-banner__img">
    																<img src="assets/images/banner-img4.jpg" alt="">
    															</div>
    															<!-- /.c-banner__img -->

    														</div>
																<!-- /.c-banner -->

															</div>
															<!-- /.c-section -->

														</div>
														<!-- /.c-section-wrapper -->

		<div id="section5" class="c-section-wrapper">

																	<div class="c-section u-padding-top-small u-padding-bottom-none">

																	<!-- 	<div class="c-video">

																			<script src="//fast.wistia.com/embed/medias/sb5jo0u2h3.jsonp" async></script><script src="//fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><div class="wistia_embed wistia_async_sb5jo0u2h3 seo=false videoFoam=true" style="height:100%;width:100%">&nbsp;</div></div></div>

																		</div> -->
																		<!-- /.c-video -->

																	</div>
																	<!-- /.c-section -->

																</div>
																<!-- /.c-section-wrapper -->

		<div id="contact" class="c-section-wrapper">

																			<div class="c-section u-padding-top-large u-padding-bottom-small">
																				<div class="o-wrapper">

																					<div class="c-section__header u-padding-bottom-large">
																						<h2 class="c-section__heading c-section__heading--light-blue editable-text">Are you ready to start the journey?</h2>
																						<h3 class="c-section__subheading editable-text">Sign up to get started. We’ll get in touch soon!</h3>
																					</div>
																					<!-- /.c-section__header -->

																					<form class="c-form" method="post">
																						<fieldset>
																							<p class="c-form__section-header">I'm interested in:</p>

																							<div class="o-layout">
																								<div class="o-layout__item u-1/1@mobile u-1/2@tablet u-1/3@desktop">
																									<label class="c-form-field">
																										<span class="c-form-field__wrapper">
																											<input type="text" name="name" value="" class="c-form-field__element" placeholder="Name" required>
																										</span>
																									</label>
																									<!-- /.c-form-field -->
																								</div>
																								<!-- /.o-layout__item -->

																								<div class="o-layout__item u-1/1@mobile u-1/2@tablet u-1/3@desktop">
																									<label class="c-form-field">
																										<span class="c-form-field__wrapper">
																											<input type="tel" name="phone" value="" class="c-form-field__element" placeholder="Phone" required>
																										</span>
																									</label>
																									<!-- /.c-form-field -->
																								</div>
																								<!-- /.o-layout__item -->

																								<div class="o-layout__item u-1/1@mobile u-1/2@tablet u-1/3@desktop">
																									<label class="c-form-field">
																										<span class="c-form-field__wrapper c-form-field__wrapper--select">
																											<span class="c-select-list">
																												<select name="company_role">
																													<option value="">Your role in company</option>
																													<option value="Architect">Architect</option>
																													<option value="Engineer">Engineer</option>
																													<option value="Manager">Manager</option>
																													<option value="Director">Director</option>
																													<option value="Executive">Executive</option>
																												</select>
																											</span> <!-- /.c-select-list -->
																										</span>
																									</label>
																									<!-- /.c-form-field -->
																								</div>
																								<!-- /.o-layout__item -->

																								<div class="o-layout__item u-1/1@mobile u-1/2@tablet u-1/3@desktop">
																									<label class="c-form-field">
																										<span class="c-form-field__wrapper">
																											<input type="text" name="company_name" value="" class="c-form-field__element" placeholder="Company">
																										</span>
																									</label>
																									<!-- /.c-form-field -->
																								</div>
																								<!-- /.o-layout__item -->

																								<div class="o-layout__item u-1/1@mobile u-1/2@tablet u-1/3@desktop">
																									<label class="c-form-field">
																										<span class="c-form-field__wrapper">
																											<input type="email" name="email" value="" class="c-form-field__element" placeholder="Email" required>
																										</span>
																									</label>
																									<!-- /.c-form-field -->
																								</div>
																								<!-- /.o-layout__item -->

																								<div class="o-layout__item u-1/1@mobile u-1/2@tablet u-1/3@desktop">
																									<label class="c-form-field">
																										<span class="c-form-field__wrapper c-form-field__wrapper--select">
																											<span class="c-select-list">
																												<select name="projects">
																													<option value="">Projects/year</option>
																													<option value="1">1 to 5</option>
																													<option value="2">6 to 20</option>
																													<option value="2">Over 20</option>
																												</select>
																											</span> <!-- /.c-select-list -->
																										</span>
																									</label>
																									<!-- /.c-form-field -->
																								</div>
																								<!-- /.o-layout__item -->

																							</div>
																							<!-- /.o-layout -->

																							<div class="o-layout">
																								<div class="o-layout__item u-1/1@mobile u-1/2@tablet">
																									<label class="c-checkbox">
                                                    <input type="checkbox" name="checkbox3" value="Newsletter" class="c-checkbox__element">
                                                    <span></span>
                                                    <span class="c-checkbox__label">The latest from Tridify to my inbox? <br> Yes please!</span>
                                                  </label>
																								</div>
																								<!-- /.o-layout__item -->

																								<div class="o-layout__item u-1/1@mobile u-1/2@tablet">
																									<div class="c-form__submit-wrapper">
																										<button class="c-btn c-btn--primary c-form__submit-btn" type="submit" name="submit_btn">Submit</button>
																									</div>
																									<!-- /.c-form__submit-wrapper -->
																								</div>
																								<!-- /.o-layout__item -->
																							</div>
																							<!-- /.o-layout -->

																						</fieldset>
																					</form>
																					<!-- /.c-form -->

                                          <?php
                                          //if "email" variable is filled out, send email
                                          if (isset($_REQUEST['email']))  {

                                            //Email information
                                            $admin_email = "no-reply@tridify.com";

                                            $name = $_REQUEST['name'];
                                            $phone = $_REQUEST['phone'];
                                            $company_role = $_REQUEST['company_role'];
                                            $company_name = $_REQUEST['company_name'];
                                            $email = $_REQUEST['email'];
                                            $projects = $_REQUEST['projects'];

                                            // $checkbox1 = $_REQUEST['checkbox1'];
                                            // $checkbox2 = $_REQUEST['checkbox2'];
                                            $checkbox3 = $_REQUEST['checkbox3'];

                                            $subject = "New website form sign up";
                                            $body = "Name: " . $name . "\r\n Phone: " . $phone . "\r\n Company role: " . $company_role . "\r\n Company: " . $company_name . "\r\n Email: " . $email . "\r\n Projects per year: " . $projects . "\r\n\r\n" . $checkbox3;

                                            //send email
                                            mail($admin_email, "$subject", $body, "From:" . $email);
                                          }
                                          ?>

                                          <div class="c-section__copy">
    																				<p class="u-text-light-blue"><a href="https://tridify.intellink.fi/distribution/tridify/">Download the Tridify app</a>. For Tridify Subscribers</p>
    																			</div>

																				</div>
																				<!-- /.o-wrapper -->
																			</div>
																			<!-- /.c-section -->

																		</div>
																		<!-- /.c-section-wrapper -->


	</main>
	<footer class="c-page-footer">
		<div class="o-wrapper">
			<div class="o-layout">
				<div class="o-layout__item u-1/1 u-2/3@desktop">
					<h3 class="c-page-footer__heading editable-text">Contact</h3>

					<div class="o-layout">
						<div class="o-layout__item u-1/1 u-1/2@tablet">
							<div class="c-page-footer__copy">
								<div class="c-page-footer__address editable">
									<span>Jarkko Hämäläinen</span><br>
									<span>CEO</span><br>
									<span>Phone: +358 50 486 9262</span><br>
									<a href="mailto:jarkko.hamalainen@tridify.com">jarkko.hamalainen@tridify.com</a>
								</div>
								<!-- /.c-page-footer__address -->

								<div class="c-page-footer__address editable">
									<span>Nigel Alexander</span><br>
									<span>Business Development, UK</span><br>
									<span>Phone: +44 7831 485 081 </span><br>
									<a href="mailto:nigel.alexander@tridify.com">nigel.alexander@tridify.com</a>
								</div>
								<!-- /.c-page-footer__address -->
							</div>
							<!-- /.c-page-footer__copy -->
						</div>
						<!-- /.o-layout__item -->

						<div class="o-layout__item u-1/1 u-1/2@tablet">
							<div class="c-page-footer__copy">
								<div class="c-page-footer__address editable">
									<span>Tomi Haverinen</span><br>
									<span>Business Development, Finland</span><br>
									<span>Phone: +358 40 5415112</span><br>
									<a href="mailto:tomi.haverinen@tridify.com">tomi.haverinen@tridify.com</a>
								</div>
								<!-- /.c-page-footer__address -->

								<div class="c-page-footer__address editable">
									<span>Alexander Le Bell</span><br>
									<span>Managing Director, Tridify Middle East</span><br>
									<span>Phone: +971 50 6680895</span><br>
									<a href="mailto:alexander.lebell@tridify.com">alexander.lebell@tridify.com</a>
								</div>
								<!-- /.c-page-footer__address -->
							</div>
							<!-- /.c-page-footer__copy -->
						</div>
						<!-- /.o-layout__item -->
					</div>
					<!-- /.o-layout -->
				</div>
				<!-- /.o-layout__item -->

				<div class="o-layout__item u-1/1 u-1/3@desktop u-flex">

          <div class="o-layout u-flex-space-between">
            <div class="o-layout__item u-1/1 u-1/2@tablet u-1/1@desktop">

              <div class="c-page-footer__copy editable">
                <div class="c-page-footer__address editable">
                  <span>Juha Alanen</span><br>
                  <span>Senior VP, Tridify Middle East</span><br>
                  <span>Phone: +971 55 1447493</span><br>
                  <a href="mailto:juha.alanen@tridify.com">juha.alanen@tridify.com</a>
                </div>
              </div>
              <!-- /.c-page-footer__copy -->

            </div>
            <!-- /.o-layout__item -->
            <div class="o-layout__item u-1/1 u-1/2@tablet u-1/1@desktop u-flex-order1@desktop">

							<h3 class="c-page-footer__heading editable-text">Follow us</h3>
							<ul class="c-social-icons o-list-inline">
								<li class="c-social-icons__item o-list-inline__item">
									<a href="https://twitter.com/tridify" target="_blank" class="c-social-icons__link"><i class="icon-tweet"></i></a>
								</li>
								<li class="c-social-icons__item o-list-inline__item">
									<a href="https://www.facebook.com/tridify" target="_blank" class="c-social-icons__link"><i class="icon-facebook"></i></a>
								</li>
								<li class="c-social-icons__item o-list-inline__item">
									<a href="https://www.linkedin.com/company-beta/10400948/" target="_blank" class="c-social-icons__link"><i class="icon-linkedin"></i></a>
								</li>
								<li class="c-social-icons__item o-list-inline__item">
									<a href="https://www.youtube.com/channel/UCW_5qST2Y04gghVUAjHxjmg" target="_blank" class="c-social-icons__link"><i class="icon-youtube"></i></a>
								</li>
							</ul>

						</div>
  					<!-- /.o-layout__item -->
					</div>
					<!-- /.o-layout -->

				</div>
				<!-- /.o-layout__item -->
			</div>
			<!-- /.o-layout -->
		</div>
		<!-- /.o-wrapper -->
	</footer>
	<!-- /.c-page-footer -->


	<!-- Scripts -->

	<script src="assets/scripts/min/main-min.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63443815-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
